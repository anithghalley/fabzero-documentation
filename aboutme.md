|[About Me](./aboutme.md) | [Git Documentation](./index.md) | [Issues Documentation](./issues.md) | [Image Compression](./image_com.md) |  [2D Tools and video](./2dtools.md) |
# About Me.

Name: Anith Ghalley.

Working at: Druk Gyalpo's Insitute.

Working As: Teacher.

Qualification: B.E Electronics and Communication Engineering.

**"** Digital fabrication is the future and I would really want to learn and practice in this field. With the learning, we can make almost anything. It would be just the matter of working hour you put in to achieve your goal. I believe that this field will not only save time and help us create cool stuff but it will also encourage creativity and customization. A best field to invest our time and effort to enhances our skills and knowledge. **"**