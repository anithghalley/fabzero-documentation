|[About Me](./aboutme.md) | [Git Documentation](./index.md) | [Issues Documentation](./issues.md) | [Image Compression](./image_com.md) |  [2D Tools and video](./2dtools.md) |

# Image Compression:

In this part of the session we learned about vector and rastor image:
- Raster images use bit maps to store information.
    - file extensions: BMP, .TIF, .GIF, .JPG

- Vector image is composed of a series of mathematical curves
    - File extensions : .SVG, .EPS, .PDF, .AI, .DXF

If we use laser to cut a raster image then instead of cutting it will engraved it due to gradient.

### **Installing FlameShot:**

This software helps to take screenshot and helps us do necessare editing on the spot.
- Command to install flameshot (Homebrew is already install)

```
brew install --cask flameshot
```

But due to something I am unable to open the Flameshot in my machine. I will keep looking at this issue to solve it.

### **Installing Imagemagick:**

Imagemagick is an CLI tool to perform image editing in a very fast and efficient way.
- Command used to install imagemagick:
```
brew install imagemagick
```
![](./images/image_com/imagemagick.png)
And then I learned that we can create a temp forlder and for time being we can dump all the documentation related images and use imagemagick to perform various image editing process.
- The folder will be listed in .gitignore, so that whenever I push my repo the temp file will not be ignored.

Command used to open the .gitignore is:
```
nano .gitignore
```
This will open a new terminal where we can add add the types of files and folders we want git to ignore while pushing the repo.

### **Task Performed Using Imagemagick.**

Command used to get the detailed list of the files and folder present in the home directory for mac is:
```
ls -lG -h
```
- REsize the image and creat a new one with compressed size.
```
convert bigimage.jpeg -resize 640x640 smallimage.jpeg
```
- Resizing the image to a particular resolution and replace the previous file.
```
mogrify -resize 640x640 bigimage2.jpeg
```
![](./images/image_com/resize.png)

- Converting an image format.
```
magick bigimage2.jpeg bigimage2new.png
```
![](./images/image_com/convert_format.png)

- Creating a compose image, using two image and generating one file with the two images side by side.
```
convert bigimage2new.png ant3.png -geometry x400 +append strip.jpg
```
![](./images/image_com/compose_image.png)

- The generated image:
![](./images/image_com/strip.jpg)
