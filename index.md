|[About Me](./aboutme.md) | [Git Documentation](./index.md) | [Issues Documentation](./issues.md) | [Image Compression](./image_com.md) |  [2D Tools and video](./2dtools.md) |
# Gitlab Documentation.

## Project in GitLab.

- Operation System Used: MacOS.

![](./images/gitlab/new_project.png)

 - Firstly a project is created on the gitlab platform names fabzero Documentation to complie and document all the work and task done in this fabzero program.

## SSH key Generation in Terminal.

```
anithghalleyacademy.bt@Aniths-Air ~ % ssh-keygen -t ed25519        
```
- when the above command is excuted on the terminal, this will help to generate ssh key for the client machine. The screenshot below presents output of the execution of the above command.

![](./images/gitlab/ssh_generation.png)

- This will generate a private and public ssh key for the machine. The keys were generate in the location indicated below.

```
/Users/anithghalleyacademy.bt/.ssh
```

- The two ssh key generated are shown below.

![](./images/gitlab/ssh_keys.png)

**"** the id_rsa and id_rsa.pub are the ssh key fro fab academy gitlab account **"**

- The public key generated is given below:
 - Then the public key is copied and add to the gitlab account so that the machine gets configure with the gitlab account.

![](./images/gitlab/ssh_added.png)

- The command below is used to Check if the machine is linked with my gitlab account.
```
ssh -T git@gitlab.com         
```
- the output of the above command showed successful linkage of gitlab and the machine.

![](./images/gitlab/check.png)

## Cloning The Project.

- After successful linkage, the command below is used to clone the Repository from gitlab to my machine. The screenshot below shows the process of cloning to my "fabzero documentation directory".

![](./images/gitlab/clone.png)

## Push the files to the Reprository.


- An Index file and a about me file is created in the project and finally all the work done is commited and pushed to the gitlab repository (fabzero documentation). The screenshot below shows the process to push the files to the repository.

![](./images/gitlab/git_push.png)
