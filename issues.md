|[About Me](./aboutme.md) | [Git Documentation](./index.md) | [Issues Documentation](./issues.md) | [Image Compression](./image_com.md) |  [2D Tools and video](./2dtools.md) |

# Issues In Gitlab.

During this session in the morning I learned about issuse, labels, milestones and Boardes in gitlab.


## Issues:

Issues will help us to come up with a solution. When ever we come up with a problem we can create an issues which can add help by assigning people and keep track of the solution.

![](./images/issues/my_issue.png)

## Labels:

Label are like tags to indicate the status of an issue. We can create new labels as per our choice. I have created two label called **Initial Phase** and **Inprogress**.

![](./images/issues/my_labels.png)

## Milestones:

Milestones allows us to organize issues and merge requests into a cohesive group, with an optional start date and an optional due date: [From GitLab Docs](https://docs.gitlab.com/ee/user/project/milestones/).

I created Design and prototyping of my final project as my milestone.

![](./images/issues/my_milestone.png)

## Boards:

Boards are usually open and closed but I have created a new board using a new label called Initial Phase to indicate that my final project is in **initial phase**.

![](./images/issues/my_Boards.png)

- And I closed one of my issus i.e **gitlab documentation** and and added my project issue to **Initial Phase**.