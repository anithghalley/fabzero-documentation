|[About Me](./aboutme.md) | [Git Documentation](./index.md) | [Issues Documentation](./issues.md) | [Image Compression](./image_com.md) |  [2D Tools and video](./2dtools.md) |

# 2D Design Tools
- The output format of 2D Design is either a Rastor or a vector Image.
- For machine we use vector images because it gives very good path for the machine to execute cut.

### Vectorizing an image:
- The Software used is inkscape:
  - Inkscape is a free and open-source vector graphics editor used to create vector images, primarily in Scalable Vector Graphics format. 

First I downloaded a rastor image from the internet.

Then imported it to the inkscape, then to convert the image to a vector image I went to Path -> Trace Bitmap.
- Under Tracing a Bitmap, there is two ways i.e Single scan (For black and white image) and Multiple scan for multicolor image.

For this work we will use single scan and under that a threshold brightness must be selected, Higher the value thicker and darker the lines becomes.
![](./images/2dandvideo/trace.png)


We also need to select dpi of the exported image, for normal printing it can be between 200 to 300 dpi but for machine to read clearly minimum dpi should be 600.
![](./images/2dandvideo/dpi.png)

The image below show a rastor image (White background) with three vector image with threshold value 900, 600 and 300.
![](./images/2dandvideo/highresjoker1.png)

The image below is exported with 900 threshold brightness.
![](./images/2dandvideo/thress9001.png)

# Youtube Download
Youtube-dl is a command line video and audio downloader.
- Youtube-dl installation command:
```
brew install youtube-dl
```
![](./images/2dandvideo/youtube-dl.png)
We can copy the link of a youtube video and download it through our terminal.
```
youtube-dl https://www.youtube.com/watch?v=IUN664s7N-c
```
![](./images/2dandvideo/video_dl.png)

It also alows us the check the various formates of a video through the video link.
```
youtube-dl -F https://youtu.be/IUN664s7N-c
```
![](./images/2dandvideo/formats.png)
# LosslessCut Software
LosslessCut is a free, platform independent Video editing software, which supports numerous audio, video and container formats. 
- With the help of this software we can easily trim videos to get our required clip.
- I downloaded a dmg file to install the software.

![](./images/2dandvideo/losslesscut.png)

To trim the required protion.
- Drag and drop the video inside Lossless cut.
- Play the video and Press 'I' to select the initial point of your required video.
- Then Press 'O' to select the end part of the video.
- Then Click export as a separte video and select the format. (I kept both vido and audio)

<video width="320" height="240" controls>
  <source src="./images/2dandvideo/trimed.mov" type="video/mov">
</video>

